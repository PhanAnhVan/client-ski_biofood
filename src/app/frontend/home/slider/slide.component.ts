import { Component, OnDestroy, OnInit } from '@angular/core';
import { Globals } from "../../../globals";

@Component({
    selector: 'app-slide',
    templateUrl: './slide.component.html',
    styleUrls: ['./slide.component.scss']
})

export class SlideComponent implements OnInit, OnDestroy {

    private connect;

    public width: number = innerWidth;

    constructor(public globals: Globals) {
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response.token) {
                case "getslide":
                    this.slide.data = response.data;                                       
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.slide.send();
    }

    ngOnDestroy() {
        if (this.connect) {
            this.connect.unsubscribe();
        }
    }

    public slide = {
        token: "api/home/slide",
        data: [],
        send: () => {
            this.globals.send({ path: this.slide.token, token: "getslide", params: { type: 1 } });
        },
    };


    public slideOptions = {
        loop: true,
        mouseDrag: true,
        touchDrag: false,
        pullDrag: false,
        dots: true,
        navSpeed: 500,
        autoplayTimeout: 5000,
        autoplaySpeed: 1000,
        autoplay: true,
        items: 1,
        nav: true,
        navText: [
            '<img src="../../../../assets/img/icon-slide-left.png" alt="Arrow left" class="img-fluid" />',
            '<img src="../../../../assets/img/icon-slide-right.png" alt="Arrow right" class="img-fluid" />',
        ],
    };
}
