// import { getRequest } from './../core/api/useRequest';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Globals } from 'src/app/globals';
import { GET_ORDER_PAGE, GET_PRODUCTS, GET_PRODUCT_PAGE } from '../core/api';
import { getOrderPage, getProductPage } from '../core/api/tokens';
import { routerProjects, routerServices, routerSolutions } from '../core/routers';
import axios from 'axios';
// import { getRequest } from '../core/api/useRequest';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
    private connect;
    private token = {
        // productGroup: 'api/getProductGroupHome',
        // productList: 'api/getProductList',
        // projects: 'api/getContent',
        // solutions: 'api/getContent',
        // services: 'api/getContent',
        // services: 'api/getContent',
    };

    public width: number = window.innerWidth;
    // public widthOwl: number;
    // hotlineBanner: string;

    order = <any>{};
    product = <any>{};

    constructor(public globals: Globals, public translate: TranslateService, public router: Router) {
        // this.hotlineBanner = `${this.globals.BASE_API_URL}public/slides/hotline-banner.gif`;

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getbanner':
                    this.slide.data = res.data;
                    break;

                case 'getAboutUs':
                    this.aboutUs.data = res.data;
                    break;

                case 'getContentHome':
                    this.content.data = res.data;
                    break;

                case getOrderPage:
                    if (res.status === 1) {
                        const { data } = res;

                        return (this.order = data);
                    }
                    break;

                case getProductPage:
                    if (res.status === 1) {
                        const { data } = res;

                        return (this.product = data);
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.globals.send({ path: GET_ORDER_PAGE, token: getOrderPage });
        
        /* Get list order */
        (async () => await this.globals._useRequest(''))().then(result => (this.order.list = result));
        
        this.globals.send({ path: GET_PRODUCT_PAGE, token: getProductPage });

        /* Get list product */
        (async () => await this.globals._useRequest(GET_PRODUCTS))().then(result => (this.product.list = result));

        this.slide.send();

        this.aboutUs.send();

        this.content.send();

        // this.widthOwl = document.getElementById('owl-carousel')?.offsetWidth / (this.width > 415 ? 5 : 2) - (this.width > 415 ? 15 : 20)
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    public aboutUs = {
        token: 'api/getAboutUs',
        data: <any>{},
        send: () => {
            this.globals.send({ path: this.aboutUs.token, token: 'getAboutUs' });
        },
    };

    public content = {
        token: 'api/getContentHome',
        data: <any>{},
        send: () => {
            this.globals.send({
                path: this.content.token,
                token: 'getContentHome',
                params: { limit: 8 },
            });
        },
    };

    public slide = {
        token: 'api/home/slide',
        data: [],
        send: () => {
            this.globals.send({ path: this.slide.token, token: 'getbanner', params: { type: 2 } });
        },
    };
}
