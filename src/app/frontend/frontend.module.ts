import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { Ng5SliderModule } from 'ng5-slider';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { BindSrcDirective } from '../services/directive/bindSrc.directive';
import { ClickOutsideDirective } from '../services/directive/clickOutside.directive';
import { BoxButtonComponent } from './components/box-button/box-button.component';
import { BoxContentGridComponent } from './components/news/box-horizontal/box-content-grid.component';
import { BoxContentComponent } from './components/news/box-vertical/box-content.component';
import { BoxProductComponent } from './components/box-product/box-product.component';
import { CommentComponent } from './components/comment/comment.component';
import { ContactComponent } from './containers/contact/contact.component';
import { NotFoundComponent } from './containers/not-found/not-found.component';
import { SearchComponent } from './containers/search/search.component';
import { routerNews, routerProduct } from './core/routers';
import { FrontendComponent } from './frontend.component';
import { HomeComponent } from './home/home.component';
import { SlideComponent } from './home/slider/slide.component';
import { DetailContentComponent } from './news/detail/detail-content.component';
import { ListContentComponent } from './news/list/content.component';
import { PageComponent } from './pages/page.component';
import { DetailProductComponent } from './product/detail/detail-product.component';
import { ListProductComponent } from './product/list/product.component';
import { SanitizeHtmlPipe } from './sanitizeHtml.pipe';
import { FooterComponent } from './shared/footer/footer.component';
import { HeaderComponent } from './shared/header/header.component';
import { MenuComponent } from './shared/menu/desktop/menu.component';
import { MenuMobileComponent } from './shared/menu/mobile/menu-mobile.component';

const appRoutes: Routes = [
    {
        path: '',
        component: FrontendComponent,
        children: [
            { path: 'trang-chu', redirectTo: '' },
            { path: '', component: HomeComponent },

            { path: `${routerProduct}`, component: ListProductComponent },
            { path: `${routerProduct}/:link`, component: ListProductComponent },
            { path: `${routerProduct}/:links/:link`, component: DetailProductComponent },

            { path: routerNews, component: ListContentComponent },
            { path: `${routerNews}/:link`, component: ListContentComponent },
            { path: `${routerNews}/:links/:link`, component: DetailContentComponent },

            { path: 'lien-he', component: ContactComponent },

            { path: 'tim-kiem', component: SearchComponent },
            { path: 'tim-kiem/:keywords', component: SearchComponent },

            { path: '404', component: NotFoundComponent },
            { path: ':link', component: PageComponent },
            { path: ':parent_link/:link', component: PageComponent },
        ],
    },
    { path: '**', redirectTo: '404' },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        CollapseModule,
        TabsModule.forRoot(),
        RouterModule.forChild(appRoutes),
        TimepickerModule.forRoot(),
        BsDatepickerModule.forRoot(),
        ModalModule.forRoot(),
        CarouselModule,
        TypeaheadModule.forRoot(),
        PaginationModule.forRoot(),
        BsDropdownModule.forRoot(),
        LazyLoadImageModule,
        Ng5SliderModule,
    ],
    declarations: [
        FrontendComponent,
        HomeComponent,
        ContactComponent,
        HeaderComponent,
        FooterComponent,
        PageComponent,
        MenuComponent,
        MenuMobileComponent,
        CommentComponent,
        DetailContentComponent,
        BoxContentComponent,
        BoxContentGridComponent,
        ListContentComponent,
        ListProductComponent,
        SanitizeHtmlPipe,
        BindSrcDirective,
        ClickOutsideDirective,
        SearchComponent,
        DetailProductComponent,
        SlideComponent,
        NotFoundComponent,
        BoxProductComponent,
        BoxButtonComponent,
    ],
})
export class FrontendModule {}
