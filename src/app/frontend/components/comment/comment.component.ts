import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core'
import { NavigationEnd, Router } from '@angular/router'

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html'
})
export class CommentComponent implements OnInit, OnChanges {
    public link = ''
    constructor(public router: Router) {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.link = window.location.href
                setTimeout(() => {
                    if (window['commentfacebook']) {
                        window['commentfacebook']()
                    }
                }, 1000)
            }
        })
    }

    ngOnInit() {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.link = window.location.href
                setTimeout(() => {
                    if (window['commentfacebook']) {
                        window['commentfacebook']()
                    }
                }, 1000)
            }
        })
    }
    ngOnChanges(changes: SimpleChanges) {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.link = window.location.href
                setTimeout(() => {
                    if (window['commentfacebook']) {
                        window['commentfacebook']()
                    }
                }, 1000)
            }
        })
    }
}
