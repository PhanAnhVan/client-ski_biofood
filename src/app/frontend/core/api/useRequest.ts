import axios from 'axios';


export const getRequest = async (api: string) => {
    const url = `https://scm.biofood.vn/${api}`;

    try {
        const res = await axios.get(url);
        const { data, status } = res.data;

        if (res.status !== 200 && status !== 1) return;

        return data;
    } catch (error) {
        console.error(error);

        throw error;
    }
};
