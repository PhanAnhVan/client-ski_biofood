import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core'
import { Router } from '@angular/router';
import { Subscription } from 'rxjs'
import { Globals } from 'src/app/globals'

@Component({
    selector: 'app-menu-mobile',
    templateUrl: './menu-mobile.component.html',
    styleUrls: ['./menu-mobile.component.scss']
})
export class MenuMobileComponent implements OnInit, OnDestroy {
    @Output('menumobile') menumobile = new EventEmitter<number>()

    public connect: Subscription

    public show: number
    public url: string = ''
    public data: any
    public menu: any

    width: number = innerWidth

    public token: any = {
        menu: 'api/getmenu'
    }

    constructor(public globals: Globals, public router: Router) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getmenu':
                    let data = this.compaid(res.data)
                    this.menu = data
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.width <= 768 &&
            this.globals.send({
                path: this.token.menu,
                token: 'getmenu',
                params: { position: 'menuMain' }
            })
    }

    compaid(data: any[]) {
        let list = []

        data = data.filter(function (item: { parent_id: string | number }) {
            let v = isNaN(+item.parent_id) && item.parent_id ? 0 : +item.parent_id
            v == 0 ? '' : list.push(item)
            return v == 0 ? true : false
        })

        let compaidmenu = (data: string | any[], skip: boolean, level = 0) => {
            level = level + 1
            if (skip == true) {
                return data
            } else {
                for (let i = 0; i < data.length; i++) {
                    let obj = []
                    list = list.filter(item => {
                        let skip = +item.parent_id == +data[i]['id'] ? false : true
                        if (skip == false) {
                            obj.push(item)
                        }
                        return skip
                    })
                    let skip = obj.length == 0 ? true : false
                    data[i]['href'] = getType(data[i]['link'], +data[i].type)
                    data[i]['level'] = level
                    data[i]['data'] = compaidmenu(obj, skip, level)
                }
                return data
            }
        }
        let getType = (link: any, type: number) => {
            switch (+type) {
                case 1:
                case 2:
                case 3:
                    link = link
                    break
                case 4:
                    link = link
                    break
                default:
                    break
            }
            return link
        }
        return compaidmenu(data, false)
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    handleItemClick = (skip: boolean, item, router = false) => {
        const navItem = document.getElementById('dropdown-menu-child-' + item.id)
        const navItemAll = document.querySelectorAll('.active-menu-child')
        const navContainer = document.getElementById('menu-mobi')

        if (item.data && item.data.length > 0 && !router) {
            skip ? navItem.classList.add('active-menu-child') : navItem.classList.remove('active-menu-child')
        } else {
            for (let i = 0; i < navItemAll.length; i++) {
                navItemAll[i].classList.remove('active-menu-child')
            }

            // const UNREDIRECT_LIST = ['services', 'solutions']
            // if (UNREDIRECT_LIST.includes(item.code)) return

            navContainer.classList.toggle('menu-mobi-hidden')

            const url =
                +item.type === 3 && item.parent_link?.length
                    ? '/san-pham/' + item.href
                    : '/' + (item.parent_link?.length ? item.parent_link + '/' + item.href : item.href)
            this.router.navigate(['/' + url])
        }
    }
}
