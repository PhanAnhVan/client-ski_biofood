import { Directive, ElementRef, EventEmitter, HostListener, Output } from '@angular/core';

@Directive({
	selector: '[clickOutside]'
})
export class ClickOutsideDirective {
	@Output() clickOutside = new EventEmitter<void>();

	constructor(private el: ElementRef) { }

	@HostListener('document:click', ['$event.target'])

	public onClick(target: any) {
		const clickedInside = this.el.nativeElement.contains(target);

		if (!clickedInside) {
			this.clickOutside.emit();
		}
	}
}
