import { Component, OnInit, OnDestroy, TemplateRef, ViewContainerRef } from '@angular/core'
import { TableService } from '../../../../services/integrated/table.service'
import { Globals } from '../../../../globals'
import { AlertComponent } from '../../../modules/alert/alert.component'
import { ToastrService } from 'ngx-toastr'
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html'
})
export class GetlistComponent implements OnInit, OnDestroy {
    public token: any = {
        getlist: 'get/attribute/getlist', //router lấy dữ liệu
        remove: 'set/attribute/remove'
    }
    modalRef: BsModalRef
    public connect: any
    public id: any
    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: '#', field: 'action', show: true },
        { title: '', field: 'status', show: true, filter: true }
    ]
    constructor(
        public globals: Globals,
        public table: TableService,
        private modalService: BsModalService,

        public toastr: ToastrService
    ) {
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response['token']) {
                case 'getList':
                    this.table._concat(response['data'], true)
                    break
                case 'removeBrand':
                    let type = response['status'] == 1 ? 'success' : response['status'] == 0 ? 'warning' : 'danger'
                    this.toastr[type](response['message'], type, { timeOut: 1000 })
                    if (response['status'] == 1) {
                        this.table._delRowData(this.id)
                    }
                    break
                default:
                    break
            }
        })
    }
    ngOnDestroy() {
        this.connect.unsubscribe()
    }
    ngOnInit() {
        this.getList()
        this.table._ini({ cols: this.cols, data: [], keyword: 'brand' })
    }
    getList() {
        this.globals.send({ path: this.token.getlist, token: 'getList' })
    }
    onRemove(item: any): void {
        this.id = item.id
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'settings.removeBrand', name: item.name } })
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'removeBrand', params: { id: item.id, name: item.images } })
            }
        })
    }
}
