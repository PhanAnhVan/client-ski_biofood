import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { Globals } from '../../../globals';
import { TableService } from '../../../services/integrated/table.service';

@Component({
    selector: 'app-group-product',
    templateUrl: './group-product.component.html',
    styleUrls: ['./group-product.component.css']
})
/**
 * diem.ntn
 * create: 14/2/2020
 */

export class GroupProductGetlistComponent implements OnInit, OnDestroy {

    public show;
    public connect;

    public id;

    @Output("filter") filter = new EventEmitter();

    public token: any = {
        getlist: "get/pages/grouptype"
    }

    modalRef: BsModalRef;

    private cols = [

        { title: 'lblName', field: 'name', show: true, filter: true },

        { title: 'lblCount', field: 'count_product', show: true, filter: true },
    ];
    public tablegroupproduct = new TableService();

    constructor(

        public globals: Globals,

    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getlistproductgroup":

                    this.tablegroupproduct._concat(res.data, true);
                    break;


                default:
                    break;
            }
        });
    }
    ngOnInit() {
        this.getlist()
    }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }
    getlist = () => {
        this.globals.send({ path: this.token.getlist, token: 'getlistproductgroup', params: { type: 3 } });
        this.tablegroupproduct._ini({ cols: this.cols, data: [] });
    }

    onCheckItem(item) {

        item.check = (item.check == true) ? false : true;

        this.filter.emit(item.id);
    }

}
