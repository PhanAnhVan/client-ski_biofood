import { Component, OnInit, OnDestroy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Globals } from '../../../globals'
import { ToastrService } from 'ngx-toastr'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
    styleUrls: ['./process.component.css']
})
export class ProcessComponent implements OnInit, OnDestroy {
    fm: FormGroup

    public id: number

    public connect

    public group: any = []

    public token: any = {
        getrow: 'get/customer/getrow',

        process: 'set/customer/process'
    }

    constructor(
        public fb: FormBuilder,
        public globals: Globals,
        public toastr: ToastrService,
        public router: Router,
        public routerAct: ActivatedRoute
    ) {
        this.routerAct.params.subscribe(params => {
            this.id = +params['id']
        })

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getrow':
                    if (res.status == 1) {
                        this.fmConfigs(res.data)
                    }
                    break

                case 'processCustomer':
                    let type = res.status == 1 ? 'success' : res.status == 0 ? 'warning' : 'danger'
                    this.toastr[type](res.message, type, { closeButton: true }, { timeOut: 1000 })
                    if (res.status == 1) {
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/customer/get-list'])
                        }, 1000)
                    }
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        if (this.id && this.id != 0) {
            this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } })
        } else {
            this.fmConfigs()
        }
    }

    ngOnDestroy() {
        if (this.connect) {
            this.connect.unsubscribe()
        }
    }

    fmConfigs(item: any = '') {
        item = typeof item === 'object' ? item : { status: 1, sex: 1 }

        this.fm = this.fb.group({
            name: [item.name ? item.name : ''],
            sex: item.sex,
            birth_date: [item.birth_date ? item.birth_date : null],
            phone: [item.phone ? item.phone : null, [Validators.required, Validators.pattern('^[0-9]*$')]],
            email: [
                item.email ? item.email : null,
                [
                    Validators.required,
                    Validators.pattern(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)
                ]
            ],
            address: [item.address ? item.address : null],
            status: item.status && item.status == 1 ? true : false
        })
    }

    onSubmit() {
        if (this.fm.valid) {
            const obj = this.fm.value
            obj.status === true ? (obj.status = 1) : (obj.status = 0)
            this.globals.send({ path: this.token.process, token: 'processCustomer', data: obj, params: { id: this.id || 0 } })
        }
    }
}
